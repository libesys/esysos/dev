set ESYS_DEV=%~dp0..\..
set ESYSOS_DEV=%~dp0..\..
set ESYSBASE_DEV=%~dp0..\..
set ESYSLOG_DEV=%~dp0..\..
set ESYSTRACE_DEV=%~dp0..\..
set DBG_LOG_DEV=%~dp0..\..
set FREERTOS_DEV=%~dp0..\..
set ESYSC_DEV=%~dp0..\..

set ESYSBASE=%~dp0..\..\src\esysbase
set ESYSFILE=%~dp0..\..\src\esysfile
set ESYSLOG=%~dp0..\..\src\esyslog
set ESYSTRACE=%~dp0..\..\src\esystrace
set ESYSMSVC=%~dp0..\..\src\esysmsvc
set ESYSOS=%~dp0..\..\src\esysos
set ESYSRES=%~dp0..\..\src\esysres
set ESYSTEST=%~dp0..\..\src\esystest

set PYSWIG=%~dp0..\..\extlib\pyswig\src\pyswig
set DBG_LOG=%~dp0..\..\extlib\dbg_log
set LOGMOD=%~dp0..\..\extlib\dbg_log
set SPDLOG=%~dp0..\..\extlib\spdlog
set FREERTOS=%~dp0..\..\extlib\freertos
set ESYSC=%~dp0..\..\extlib\esysc
