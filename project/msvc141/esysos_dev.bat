@echo off

call "%~dp0\..\msvc\set_env.bat"
call "%~dp0\..\msvc\create_python_env.bat"

@echo on
python -m pip install -r "%~dp0\..\msvc\requirements.txt"
call "%~dp0\conan_install.bat"

@echo off
"%ESYSSDK_INST_DIR%\bin\esyssdkcli" --call_msvc="2017" "%~dp0\esysos_dev.sln"
