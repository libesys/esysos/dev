#!/bin/bash

echo "${TXT_S}Boostrap ...${TXT_CLEAR}"

PATH="$HOME/.local/bin:$PATH"
PATH=~/bin:$PATH

mkdir build_dev
cd build_dev
repo init -u ssh://git@gitlab.com/libesys/esysos/manifest.git
RESULT=$?

if [ ! ${RESULT} -eq 0 ]; then
    echo "${TXT_E}Repo init failed.${TXT_CLEAR}"
    exit 1
fi

if [ "$(uname)" == "Darwin" ]; then
    N=`sysctl -n hw.logicalcpu`
else
    N=`nproc --all`
fi

repo sync -j$N
RESULT=$?
if [ ! ${RESULT} -eq 0 ]; then
    echo "${TXT_W}Repo sync failed.${TXT_CLEAR}"
fi

echo "${TXT_S}Boostrap Done.${TXT_CLEAR}"
