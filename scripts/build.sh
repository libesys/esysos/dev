#!/bin/bash

echo "${TXT_S}Build ESysOS dev ...${TXT_CLEAR}"

cd build_dev

if [ ! -z "$1" ]; then
    echo "Checkout $1 ..."
    git fetch
    git checkout $1
    echo "Checkout done."
fi

mkdir -p build/cmake
cd build/cmake
pwd

cmake ../..
RESULT=$?
if [ ! ${RESULT} -eq 0 ]; then
   echo "${TXT_E}CMake configuration failed.${TXT_CLEAR}"
   exit 1
fi

if [ "$(uname)" == "Darwin" ]; then
    N=`sysctl -n hw.logicalcpu`
else
    N=`nproc --all`
fi

make -j$N
RESULT=$?
if [ ! ${RESULT} -eq 0 ]; then
   echo "${TXT_E}ESysOS dev failed.${TXT_CLEAR}"
   exit 1
fi

echo "${TXT_S}ESysOS dev done.${TXT_CLEAR}"
